<?php

/*
 * Copyright (C) 2016 Divecheck, All Rights Reserved.
 *
 * This file is part of Divecheck software.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace Divecheck\Config\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="system_config_data",uniqueConstraints={@ORM\UniqueConstraint(name="UNQ_path", columns={"path"})})
 */
class Config
{
    const CONFIG_SCOPE_DEFAULT = 'default';

    /**
     * @ORM\Id
     * @ORM\Column(name="config_id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     *
     * @var string
     */
    protected $scope = 'default';

    /**
     * @ORM\Column(name="scope_id", type="integer", nullable=false)
     *
     * @var integer
     */
    protected $scopeId = 0;

    /**
     * @ORM\Column(type="string", nullable=false)
     *
     * @var string
     */
    protected $path;

    /**
     * @ORM\Column(type="string", nullable=false)
     *
     * @var string
     */
    protected $value;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Config
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @param string $scope
     *
     * @return Config
     */
    public function setScope($scope = self::CONFIG_SCOPE_DEFAULT)
    {
        //        $this->scope = $scope;
        return $this;
    }

    /**
     * @return int
     */
    public function getScopeId()
    {
        return $this->scopeId;
    }

    /**
     * @param int $scopeId
     *
     * @return Config
     */
    public function setScopeId($scopeId = 0)
    {
        //        $this->scopeId = $scopeId;
        return $this;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     *
     * @return Config
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return Config
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }
}
